https://katharharshal1.medium.com/setup-nginx-ingress-controller-on-kubernetes-cluster-dd48b2b1ab61

```
git clone https://github.com/nginxinc/kubernetes-ingress.git
cd kubernetes-ingress/deployments/
kubectl apply -f common/ns-and-sa.yaml
kubectl apply -f rbac/rbac.yaml
kubectl apply -f deployment/nginx-ingress.yaml
kubectl get pods --namespace=nginx-ingress

kubectl apply -f service/nodeport.yaml
kubectl get svc --namespace=nginx-ingress

Nginx-config.yaml file--->

```
kind: ConfigMap
apiVersion: v1
metadata:
  name: nginx-config
  namespace: nginx-ingress
data:
  proxy-protocol: "True"
  real-ip-header: "proxy_protocol"
  set-real-ip-from: "0.0.0.0/0"
  ```
kubectl apply -f common/nginx-config.yaml


```
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install ingress gitlab/gitlab-agent \
    --namespace nginx-ingress \
    --create-namespace \
    --set image.tag=v16.4.0 \
    --set config.token=glagent-nbYsvfLByY3DxWzsgkvoX8sTxYCW55rLBGxBCSJ4gqP6pxL3Cg \
    --set config.kasAddress=wss://kas.gitlab.com
```

```
Error from server (InternalError): error when creating "manifest/ingress.yaml": Internal error occurred: failed calling webhook "validate.nginx.ingress.kubernetes.io": failed to call webhook: Post "https://nginx1-ingress-nginx-controller-admission.nginx-ingress.svc:443/networking/v1/ingresses?timeout=10s": service "nginx1-ingress-nginx-controller-admission" not found

kubectl get validatingwebhookconfigurations
kubectl describe validatingwebhookconfigurations <WEBHOOK_NAME>
kubectl delete validatingwebhookconfigurations <WEBHOOK_NAME>

```

```
Test ingress working or not:-

kubectl get svc -n ingress-nginx 

NAME                                                    TYPE           CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE

bollywood-svc                                           NodePort       10.107.53.155    <none>        8080:32442/TCP               41m
hollywood-svc                                           NodePort       10.101.1.65      <none>        8080:32443/TCP               41m
ingress-controller-ingress-nginx-controller             LoadBalancer   10.107.174.71    localhost     80:31134/TCP,443:31259/TCP   96m
ingress-controller-ingress-nginx-controller-admission   ClusterIP      10.100.80.187    <none>        443/TCP                      96m
tollywood-svc                                           NodePort       10.104.163.24    <none>        8080:32444/TCP               41m
web                                                     NodePort       10.110.108.238   <none>        8080:30101/TCP               57m

curl http://hello-world.info:30101/

Here http://hello-world.info is defined in ingress.yaml and 30101 coming from web service of type Nodeport

Add below host entry in host file of macbook
127.0.0.1 hello-world.info
127.0.0.1 bollywood.movies.com
127.0.0.1 hollywood.movies.com
sudo vi /etc/hosts

 curl bollywood.movies.com:32442
 curl hollywood.movies.com:32443

```
```
Test Service Internally

Try accessing the service from within the cluster:
kubectl run -i --tty --rm debug --image=busybox -- sh
wget -O- tollywood-svc.ingress-nginx:8080
wget -O- bollywood-svc.ingress-nginx:8080
wget -O- hollywood-svc.ingress-nginx:8080
```
